import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        let controller: FlutterViewController = window.rootViewController as! FlutterViewController
        let channelName = "poc/ios"
        let pocChannel = FlutterMethodChannel(name: channelName, binaryMessenger: controller.binaryMessenger)

        pocChannel.setMethodCallHandler { call, result in
            if call.method == "1" {
                result("1")
            } else if call.method == "2" {
                self.plusData { data in
                    result(data)
                }
            }
        }

        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    private func plusData(completion: @escaping (_ result: String) -> ()) {
        completion("2")
    }

}
