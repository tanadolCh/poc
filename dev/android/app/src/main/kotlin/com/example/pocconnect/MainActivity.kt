package com.example.pocconnect

import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES

class MainActivity: FlutterActivity() {
      private val CHANNEL = "poc/android"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
        // This method is invoked on the main thread.
        call, result ->
            if (call.method == "plusOne") {
                val currentValue: Int? = call.argument("value")
                val newValue = plusOne(currentValue)
                result.success(newValue)
            } else {
                result.notImplemented()
            }
            
        }
    }
    
    private fun plusOne(currentValue: Int?): Int {
        if(currentValue != null){
            return currentValue + 1
        }

        return 1
    }
}
